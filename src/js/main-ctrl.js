'use strict';

var Util = require('./util');

// @ngInject
module.exports = ($scope, searchSvc) => {

  $scope.hits = [];
  $scope.selected = -1;


  // MODAL STUFF
  // =========== -------------------------------------------------------

  $scope.openDetails = (entry) => {
    $scope.selected = entry;
    $('#details').openModal();
  };
  $scope.closeDetails = () => {
    $('#details').closeModal();
    $scope.selected = -1;
  };


  // SEARCH METHOD
  // ============= -----------------------------------------------------

  $scope.search = (term) => {

    //clear results on empty input
    if(term === '') {
      $scope.hits = [];
      return;
    }

    searchSvc.search(term).then((blob) => {
      if(blob.data.Response === 'True'){

        //prevent duplicates
        var exists = $scope.hits.filter((h) => blob.data.imdbID === h.id);
        if(exists.length !== 0) return;

        //set values of response
        if(blob.data.Poster === 'N/A') blob.data.Poster = '';
        var newEntry = {
            id: blob.data.imdbID
          , title: blob.data.Title
          , poster: blob.data.Poster
          , content: blob.data.Plot
          , link: 'http://www.imdb.com/title/' + blob.data.imdbID
          , seasons: []
        };

        //get season data
        let id = blob.data.imdbID;
        searchSvc.getSeasons(id, 1, newEntry.seasons);

        //add to results list
        if(blob.data.Poster)
          $scope.hits.push(newEntry);
      }
    });
  };

  // HELPERS
  // === ---------------------------------------------------------------
  $scope.getRange = new Util('getRange');

  // END
  // === ---------------------------------------------------------------

};
