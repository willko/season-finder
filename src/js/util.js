'use strict';

// UTIL METHODS:
// ============= -----------------------------------------------------

var _getRange = (len) => {
  var res = [];
  len = parseInt(len);
  if( (typeof len) === 'number')
    for(var n=1; n <= len; n++)
      res.push(n);
  return res;
};


// THE MODULE:
// =========== -------------------------------------------------------

class Util {
  constructor(wanted='all') {
    switch(wanted.toLowerCase()){

      /* all methods */
      case 'all':
        return {
            getRange: _getRange
        };

      /* or individually by name */
      case 'getrange':
        return _getRange;


      /* bad calls get nada */
      default:
        return;
    }
  }
}

module.exports = Util;

// END
// === ---------------------------------------------------------------
