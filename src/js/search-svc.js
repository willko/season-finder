'use strict';

//@ngInject
module.exports = ($http) => {

  const TITLE_SEARCH = 'http://www.omdbapi.com/?t='
      , ID_SEARCH = 'http://www.omdbapi.com/?i='
      , service = {};
/**
 * Searches for shows with matching term.
 * @param {string} term - The title of the show to search.
 * @return {Promise} thenable, with data in callback param.
 */
  service.search = (term) => {
    if(term !== ''){
      term = angular.lowercase( term.replace(/[ ]/g, '%20') );
      return $http.get(TITLE_SEARCH +  term + '&type=series');
    }
  };

/**
 * Recursively gets season data for show with id.
 * @param {number} id - The id of the show.
 * @param {number} season - The desired season to retrieve.
 * @param {Array[]} seasons - The array to add responses to
 */
  service.getSeasons = (id, season, seasons) => {
    $http.get(ID_SEARCH + id + '&Season=' + season).then((d)=>{
      if(d.data.Response === 'True'){
        seasons.push(d.data.Episodes);
        service.getSeasons(id, season + 1, seasons);
      }
    });
  };

  return service;
};